(function() {

    function getBrandsList(query, options, helpers, cb) {

        helpers.getDbClient(function(error, dbClient) {
            if (!error) {
                let Brands = dbClient.collection('brands');

                Brands.find(query).toArray(function(retrievalErr, brands) {
                    if (!retrievalErr) {
                        helpers.execute(cb, [brands]);
                    }
                });

            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }

    function postBrandsList(query, options, helpers, cb) {

        helpers.postDbClient(function(error, dbClient) {
            if (!error) {
                let Brands = dbClient.collection('brands');
                var bname = req.body.brand_name;
                var date = req.body.created_date;
                Brands.insert({"brand_name":bname,"created_date":date}) (function(retrievalErr, brands) {
                    if (!retrievalErr) {
                        helpers.execute(cb, [brands]);
                    }
                });

            } else {
                cb({
                    "error": "Error while connecting to mongoDB"
                })
            }
        })
    }



    exports.Brands = {
        getBrandsList: getBrandsList
    }

})();